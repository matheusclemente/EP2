package produto;

public class Produto {

    String nome;
    float preco;
    String tipo;
    int quantidade;
    int quantidade_minima;

    public Produto() {
        setNome("-vazio-");
        setPreco(0.0f);
        setTipo("Não informado");
        setQuantidade(0);
        setQuantidade_minima(0);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getQuantidade_minima() {
        return quantidade_minima;
    }

    public void setQuantidade_minima(int quantidade_minima) {
        this.quantidade_minima = quantidade_minima;
    }

    public void adicionaQuantidade(int quantidade_adicional) {
        quantidade += quantidade_adicional;
    }

}
